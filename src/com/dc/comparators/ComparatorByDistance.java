package com.dc.comparators;

import java.util.Comparator;

import com.dc.model.Distance;
import com.dc.model.TimeSpend;

public class ComparatorByDistance implements Comparator<Distance> {

	@Override
	public int compare(Distance o1, Distance o2) {
		if (o1.getDistance() > o2.getDistance())
			return -1;
		else if (o1.getDistance() < o2.getDistance())
			return -1;
		else
			return 0;
	}

}

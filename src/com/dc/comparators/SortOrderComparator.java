package com.dc.comparators;

import java.util.Comparator;

import com.dc.model.TimeSpend;

public class SortOrderComparator implements Comparator<TimeSpend>{

	@Override
	public int compare(TimeSpend o1, TimeSpend o2) {
		if(o1.getSortOrder()>o2.getSortOrder())
			return -1;
		else
			return 1;
	}

}

package com.dc.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.dc.model.SampleRaw;

public class ID3Helper {

	private ArrayList<SampleRaw> sampleList ;
	private ArrayList<Integer> targetAttList ;
	private ArrayList<String> selectedAttList ;
	private int TOTAL_CLASS=8;
	private ArrayList<ArrayList<String>> attValueList=new ArrayList<ArrayList<String>>();
	private int checkAllFromSameClass()
	{
		int c=-1;
		int size=sampleList.size();
		boolean isAllSame=true;
		if (size > 0) {
			int initial=sampleList.get(0).getCgpa();
			for (int i = 1; i < size; i++) {
				if(sampleList.get(i).getCgpa()!=initial)
				{
					isAllSame=false;
					break;
				}
			}
			if(isAllSame)
				c=initial;
		}
		return c;
		
	}
	private int getMostCommonTargetAttribute()
	{
		HashMap<Integer,Integer> hashMap=new HashMap<Integer, Integer>();
		int size=sampleList.size();
		for(int i=0;i<size;i++)
		{
			int c=sampleList.get(i).getCgpa();
			if(hashMap.containsKey(c))
			{
				int val=hashMap.get(c);
				hashMap.put(c,val+1);
			}
			else
			{
				hashMap.put(c,1);
			}
		}
		int max=-1;
		int max_class=-1;

		for(int key: hashMap.keySet())
		{
			int val=hashMap.get(key);
			if(val>max)
			{
				max=val;
				max_class=key;
			}
		}
		return max_class;
		
	}
	public int  getBestAttribute()
	{
		return 0;
	}
	private double getExampleEntropy()
	{
		ArrayList<Integer> countList=new ArrayList<Integer>(Collections.nCopies(TOTAL_CLASS, 0));
		
		int size=sampleList.size();
		
		for(int i=0;i<size;i++)
		{
			int index=sampleList.get(i).getCgpa();
			int val=countList.get(index);
			val++;
			countList.set(index,val);
		}
		double entrpy=0;
		for(int i=0;i<TOTAL_CLASS;i++)
		{
			entrpy+=-(countList.get(i)/size)*get2baseLog(countList.get(i)/size);
		}
		return entrpy;
	}
	private double get2baseLog(double value)
	{
		if(value==0)
			return 0;
		double logs=Math.log(value)/Math.log(2);
		//System.out.println("2base log"+ logs+" input: "+ value);
		return logs;
	}
}

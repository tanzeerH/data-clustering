package com.dc.utility;

import java.util.ArrayList;

public class Tablelist {
	
	private ArrayList<String> tableList=new ArrayList<String>();
	
	
	public Tablelist() {
		
		
	}
	public ArrayList<String> getTableList()
	{
		tableList.add("timezone");
		tableList.add("gender");
		tableList.add("batch");
		tableList.add("department");
		tableList.add("residence");
		tableList.add("hall");
		tableList.add("time_to_reach_hall");
		tableList.add("environment_of_hall");
		tableList.add("cgpa");
		tableList.add("cgpa_trend");
		tableList.add("did_tuition");
		tableList.add("tuition_time");
		tableList.add("tuition_reasons");
		tableList.add("did_co_carriculer");
		tableList.add("co_carriculer_time");
		tableList.add("co_carriculer_type");
		tableList.add("seriousness_type");
		tableList.add("good_result_reasons");
		tableList.add("less_study_reasons");
		tableList.add("seggesion_about_buet");
		tableList.add("buet_most_timespend_style");
		tableList.add("close_friend_in_dept");
		tableList.add("buet_most_time_spend_people");
		tableList.add("involved_in_politics");
		tableList.add("politics_effect");
		tableList.add("comment_good_result");
		tableList.add("comment_bad_restult");
		
		return tableList;
	}
	

}

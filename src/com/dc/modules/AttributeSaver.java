package com.dc.modules;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.dc.database.DatabaseHelper;
import com.dc.utility.Constants;

public class AttributeSaver {
	
	private ArrayList<ArrayList<String>> attList=new ArrayList<ArrayList<String>>();
	
	public AttributeSaver() {
		new DatabaseHelper().createDatabaseAndTables();
		createArrayList();
		readRawFile();
	}
	private void createArrayList()
	{
		for(int i=0;i<Constants.COLUMN_NO;i++)
		{
			ArrayList<String> list=new ArrayList<String>();
			attList.add(list);
		}
	}
	private void readRawFile()
	{
		int count=0;
		BufferedReader br = null;
		try {

			String sCurrentLine;
			String totalLine="";
			br = new BufferedReader(new FileReader("data_buet.csv"));
			while ((sCurrentLine = br.readLine()) != null) {
				
				String[] values=sCurrentLine.split("\",");
				//System.out.println(""+values.length);
				if(values.length<Constants.COLUMN_NO)
				{
					//System.out.println(sCurrentLine);
					totalLine=totalLine+sCurrentLine;
					
				}
				else
					totalLine=sCurrentLine;
			    
				values=totalLine.split("\",");
				if(values.length>=Constants.COLUMN_NO)
				{
					values=totalLine.split("\",");
					System.out.println("T "+values.length);
					for(int j=0;j<Constants.COLUMN_NO;j++)
						addAttributeinList(values[j], j);
					totalLine="";
					count++;
					//creating maps
					
				}
					
				if(count>200)
					break;
				
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		//printAttributes();
		saveAttributes();
	}
	private  void printAttributes()
	{
		for(int i=0;i<Constants.COLUMN_NO;i++)
		{
			System.out.println("Attribute "+i+ " Size: "+ attList.get(i).size());
			if(i==1){
			for(int j=0;j<attList.get(i).size();j++)
			{
				System.out.println(attList.get(i).get(j));
			}
			System.out.println("\n\n");}
		}
	}
	private void addAttributeinList(String input,int index)
	{
		if(input.length()>0)
		{
			if(!attList.get(index).contains(input))
				attList.get(index).add(input);
		}
		
	}
	private void saveAttributes()
	{
		DatabaseHelper dbHelper=new DatabaseHelper();
		for(int i=0;i<Constants.COLUMN_NO;i++)
			dbHelper.insertDataIntoDatabase(i,attList.get(i));
			
	}
	
}

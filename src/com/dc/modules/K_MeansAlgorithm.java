package com.dc.modules;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import com.dc.database.DatabaseHelper;
import com.dc.model.NominalKeys;
import com.dc.model.Sample;
import com.dc.model.Student;
import com.dc.model.TimeSpend;
import com.dc.utility.Constants;

public class K_MeansAlgorithm {

	private HashMap<String, Integer> tuitionTimeMap = new HashMap<String, Integer>();
	private HashMap<String, Integer> coCarriculerTimeMap = new HashMap<String, Integer>();
	private ArrayList<Sample> sampleList = new ArrayList<Sample>();
	private ArrayList<Sample> clustList = new ArrayList<Sample>();

	private double CGPA_MEAN = 3.3511484999999994;
	private double CGPA_STD_DEVIATIONS = 0.3867328811954706;

	private int TOTAL_NEUMERIC_ORDINAL_ATTRIBUTES = 3;
	private ArrayList<Double> maxList = new ArrayList<Double>(
			Collections.nCopies(TOTAL_NEUMERIC_ORDINAL_ATTRIBUTES, -100.0));

	private ArrayList<Double> minList = new ArrayList<Double>(
			Collections.nCopies(TOTAL_NEUMERIC_ORDINAL_ATTRIBUTES, -100.0));

	int count = 0;

	private int NUM_OF_CLUSTERS = 3;
	private int TOTAL_COUNT = 0;

	private int INDEX_IN_MAX_MIN_LIST_CGPA = 0;
	private int INDEX_IN_MAX_MIN_LIST_TUITIONTIME = 1;
	private int INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME = 2;

	private ArrayList<Integer> clusterCountList;
	private ArrayList<ArrayList<Double>> attributeSumList;

	ArrayList<HashMap<String, HashMap<String, Integer>>> counterHashmapList;

	public K_MeansAlgorithm() {
		System.out.println("consider changing mean eveytime");
		readTablesWithSortOrder();
		// initDataStructures();
		readRawFileForParsing();
		runKmeansAlgorithm();
	}

	private ArrayList<HashMap<String, HashMap<String, Integer>>> getHashMapList() {
		ArrayList<HashMap<String, HashMap<String, Integer>>> counterHashmapList = new ArrayList<HashMap<String, HashMap<String, Integer>>>();
		for (int i = 0; i < NUM_OF_CLUSTERS; i++) {
			HashMap<String, HashMap<String, Integer>> hashMap = new HashMap<String, HashMap<String, Integer>>();
			counterHashmapList.add(hashMap);
		}
		return counterHashmapList;
	}

	private ArrayList<ArrayList<Double>> getAttributeSumList() {
		ArrayList<ArrayList<Double>> attlist = new ArrayList<ArrayList<Double>>();

		for (int i = 0; i < NUM_OF_CLUSTERS; i++) {
			ArrayList<Double> list = new ArrayList<Double>(Collections.nCopies(
					TOTAL_NEUMERIC_ORDINAL_ATTRIBUTES, 0.0));
			attlist.add(list);
		}
		return attlist;
	}

	private void readTablesWithSortOrder() {
		DatabaseHelper dbhHelper = new DatabaseHelper();

		tuitionTimeMap = dbhHelper.raedTableWithSortOrder("tuition_time");
		// System.out.println("" + tuitionTimeMap.size());

		coCarriculerTimeMap = dbhHelper
				.raedTableWithSortOrder("co_carriculer_time");
		// System.out.println("" + coCarriculerTimeMap.size());
	}

	private void readRawFileForParsing() {
		count = 0;
		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader("clean_data.csv"));
			while ((sCurrentLine = br.readLine()) != null) {
				if (count != 0) {
					String[] values = sCurrentLine.split(",", -1);
					Sample sample = new Sample();
					sampleList.add(sample);
					for (int i = 0; i < values.length; i++)
						normalizeOrdinalAttributes(values[i], i, count - 1);
					// System.out.println("" + values.length);

				}
				count++;
				if (count > 400)
					break;

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		TOTAL_COUNT = count;
		// printSamples();
		// printMaxMins();

	}

	private void printSamples() {
		int size = sampleList.size();
		for (int i = 0; i < 10; i++) {
			Sample sample = sampleList.get(i);
			String str = "" + sample.getGender() + "  " + sample.getBatch()
					+ "  " + sample.getResidence() + "   " + sample.getCgpa()
					+ "  " + sample.getDidTuition() + "  "
					+ sample.getTuitionTime() + "  "
					+ sample.getDidExtraCarriculer() + "   "
					+ sample.getExtraCarriculerTime() + "  "
					+ sample.getCloseFriendInDept();
			System.out.println(str);

		}
	}

	private void printMaxMins() {
		for (int i = 0; i < TOTAL_NEUMERIC_ORDINAL_ATTRIBUTES; i++) {
			System.out.println("max: " + maxList.get(i) + " min: "
					+ minList.get(i));
		}
	}

	private void runKmeansAlgorithm() {
		generateInitialClusterCenters();

		// intitializing data structures for updating clusters after loop
		// completion
		for (int i = 0; i < 10; i++) {
			counterHashmapList = getHashMapList();
			clusterCountList = new ArrayList<Integer>(Collections.nCopies(
					NUM_OF_CLUSTERS, 0));
			attributeSumList = getAttributeSumList();

			// finish inittalizing

			int sampleSize = sampleList.size();
			for (int sampleIndex = 0; sampleIndex < sampleSize; sampleIndex++) {
				double minDistance = 100000;
				int minClusterIndex = -1;
				for (int clusterIndex = 0; clusterIndex < NUM_OF_CLUSTERS; clusterIndex++) {
					double distance = getDistance(clustList.get(clusterIndex),
							sampleList.get(sampleIndex));
					// System.out.println("distance: "+ distance);
					if (distance < minDistance) {
						minDistance = distance;
						minClusterIndex = clusterIndex;
					}

				}
				// System.out.println("min clusterIndex: " +
				// minClusterIndex+" min cluseter distance: "+ minDistance);
				updateClusterStats(sampleList.get(sampleIndex), minClusterIndex);
			}
			updateClusterCenters();
			//printCluseterCenters();
			printInterClasterDiffernces();
		}
		printCluseterCenters();
	}

	private void printInterClasterDiffernces() {

		System.out.println("0 & 1 distance: "+getDistance(clustList.get(0),clustList.get(1)));
		System.out.println("1 & 2 distance: "+getDistance(clustList.get(1),clustList.get(2)));
		System.out.println("0 & 2 distance: "+getDistance(clustList.get(0),clustList.get(2)));
		System.out.println();
	}

	private void printCluseterCenters() {
		int size = clustList.size();
		for (int i = 0; i < size; i++) {
			Sample sample = clustList.get(i);
			String str = "" + sample.getGender() + "  " + sample.getBatch()
					+ "  " + sample.getResidence() + "   " + (sample.getCgpa()*CGPA_STD_DEVIATIONS+CGPA_MEAN)
					+ "  " + sample.getDidTuition() + "  "
					+ sample.getTuitionTime() + "  "
					+ sample.getDidExtraCarriculer() + "   "
					+ sample.getExtraCarriculerTime() + "  "
					+ sample.getCloseFriendInDept()+" count: "+ clusterCountList.get(i);
			System.out.println(str);

		}
	}

	private void updateClusterCenters() {
		for (int i = 0; i < NUM_OF_CLUSTERS; i++) {
			Sample cluster = clustList.get(i);
			ArrayList<Double> sumList = attributeSumList.get(i);
			HashMap<String, HashMap<String, Integer>> hashMapClustr = counterHashmapList
					.get(i);
			double count = clusterCountList.get(i);

			// updateing numemaric_and_ordinal values
			double attVlaue = sumList.get(INDEX_IN_MAX_MIN_LIST_CGPA);
			attVlaue = attVlaue / count;
			// System.out.println("sum"+
			// sumList.get(INDEX_IN_MAX_MIN_LIST_CGPA)+" count "+ count);
			cluster.setCgpa(attVlaue);

			attVlaue = sumList.get(INDEX_IN_MAX_MIN_LIST_TUITIONTIME);
			attVlaue = attVlaue / count;
			cluster.setTuitionTime(attVlaue);

			attVlaue = sumList.get(INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME);
			attVlaue = attVlaue / count;
			cluster.setExtraCarriculerTime(attVlaue);

			NominalKeys nominalKeys = new NominalKeys();

			// set gender

			HashMap<String, Integer> attHashMap = hashMapClustr.get(nominalKeys
					.getKEY_GENDER());
			int max = -1;
			String max_att_value = "";
			for (String key_att : attHashMap.keySet()) {
				int val = attHashMap.get(key_att);
				if (val > max) {
					max = val;
					max_att_value = key_att;
				}
			}
			cluster.setGender(max_att_value);

			// set batch

			attHashMap = hashMapClustr.get(nominalKeys.getKEY_BATCH());
			max = -1;
			max_att_value = "";
			for (String key_att : attHashMap.keySet()) {
				int val = attHashMap.get(key_att);
				if (val > max) {
					max = val;
					max_att_value = key_att;
				}
			}
			cluster.setBatch(max_att_value);

			// set residence

			attHashMap = hashMapClustr.get(nominalKeys.getKEY_RESIDENCE());
			max = -1;
			max_att_value = "";
			for (String key_att : attHashMap.keySet()) {
				int val = attHashMap.get(key_att);
				if (val > max) {
					max = val;
					max_att_value = key_att;
				}
			}
			cluster.setResidence(max_att_value);

			// set did tuition

			attHashMap = hashMapClustr.get(nominalKeys.getKEY_DID_TUITION());
			max = -1;
			max_att_value = "";
			for (String key_att : attHashMap.keySet()) {
				int val = attHashMap.get(key_att);
				// System.out.println("did tuition val "+ key_att+" count "+
				// val);
				if (val > max) {
					max = val;
					max_att_value = key_att;
				}
			}
			cluster.setDidTuition(max_att_value);

			// set did extra carriculer

			attHashMap = hashMapClustr.get(nominalKeys.getKEY_DID_TUITION());
			max = -1;
			max_att_value = "";
			for (String key_att : attHashMap.keySet()) {
				int val = attHashMap.get(key_att);
				if (val > max) {
					max = val;
					max_att_value = key_att;
				}
			}
			cluster.setDidExtraCarriculer(max_att_value);

			// set close friend in dept

			attHashMap = hashMapClustr.get(nominalKeys
					.getKEY_HAD_FRIEND_IN_DEPT());
			max = -1;
			max_att_value = "";
			for (String key_att : attHashMap.keySet()) {
				int val = attHashMap.get(key_att);
				if (val > max) {
					max = val;
					max_att_value = key_att;
				}
			}
			cluster.setCloseFriendInDept(max_att_value);

		}

	}

	private void updateClusterStats(Sample example, int clusterIndex) {
		int count = clusterCountList.get(clusterIndex);
		clusterCountList.set(clusterIndex, count + 1);

		double sum = attributeSumList.get(clusterIndex).get(
				INDEX_IN_MAX_MIN_LIST_CGPA);
		sum += example.getCgpa();

		// .out.println("sum+ " +sum);
		attributeSumList.get(clusterIndex).set(INDEX_IN_MAX_MIN_LIST_CGPA, sum);

		sum = attributeSumList.get(clusterIndex).get(
				INDEX_IN_MAX_MIN_LIST_TUITIONTIME);
		sum += example.getTuitionTime();
		attributeSumList.get(clusterIndex).set(
				INDEX_IN_MAX_MIN_LIST_TUITIONTIME, sum);
		// System.out.println("sum+ " +sum);

		sum = attributeSumList.get(clusterIndex).get(
				INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME);
		sum += example.getExtraCarriculerTime();
		attributeSumList.get(clusterIndex).set(
				INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME, sum);
		// System.out.println("sum+ " +sum);

		HashMap<String, HashMap<String, Integer>> hashMapKeys = counterHashmapList
				.get(clusterIndex);

		// set gender
		NominalKeys nominalKeys = new NominalKeys();

		if (hashMapKeys.containsKey(nominalKeys.getKEY_GENDER())) {
			HashMap<String, Integer> attHashMap = hashMapKeys.get(nominalKeys
					.getKEY_GENDER());
			if (attHashMap.containsKey(example.getGender())) {
				int num = attHashMap.get(example.getGender());
				attHashMap.put(example.getGender(), num + 1);
			} else {
				attHashMap.put(example.getGender(), 1);
			}
		} else {
			HashMap<String, Integer> attHashMap = new HashMap<String, Integer>();
			attHashMap.put(example.getGender(), 1);
			hashMapKeys.put(nominalKeys.getKEY_GENDER(), attHashMap);

		}

		// set batch

		if (hashMapKeys.containsKey(nominalKeys.getKEY_BATCH())) {
			HashMap<String, Integer> attHashMap = hashMapKeys.get(nominalKeys
					.getKEY_BATCH());
			if (attHashMap.containsKey(example.getBatch())) {
				int num = attHashMap.get(example.getBatch());
				attHashMap.put(example.getBatch(), num + 1);
			} else {
				attHashMap.put(example.getBatch(), 1);
			}
		} else {
			HashMap<String, Integer> attHashMap = new HashMap<String, Integer>();
			attHashMap.put(example.getBatch(), 1);
			hashMapKeys.put(nominalKeys.getKEY_BATCH(), attHashMap);

		}
		// set residence

		if (hashMapKeys.containsKey(nominalKeys.getKEY_RESIDENCE())) {
			HashMap<String, Integer> attHashMap = hashMapKeys.get(nominalKeys
					.getKEY_RESIDENCE());
			if (attHashMap.containsKey(example.getResidence())) {
				int num = attHashMap.get(example.getResidence());
				attHashMap.put(example.getResidence(), num + 1);
			} else {
				attHashMap.put(example.getResidence(), 1);
			}
		} else {
			HashMap<String, Integer> attHashMap = new HashMap<String, Integer>();
			attHashMap.put(example.getResidence(), 1);
			hashMapKeys.put(nominalKeys.getKEY_RESIDENCE(), attHashMap);

		}

		// set did tuition

		if (hashMapKeys.containsKey(nominalKeys.getKEY_DID_TUITION())) {
			HashMap<String, Integer> attHashMap = hashMapKeys.get(nominalKeys
					.getKEY_DID_TUITION());
			if (attHashMap.containsKey(example.getDidTuition())) {
				int num = attHashMap.get(example.getDidTuition());
				attHashMap.put(example.getDidTuition(), num + 1);
			} else {
				attHashMap.put(example.getDidTuition(), 1);
			}
		} else {
			HashMap<String, Integer> attHashMap = new HashMap<String, Integer>();
			attHashMap.put(example.getDidTuition(), 1);
			hashMapKeys.put(nominalKeys.getKEY_DID_TUITION(), attHashMap);

		}

		// set did extra carriculer

		if (hashMapKeys.containsKey(nominalKeys.getKEY_DID_EXTRA_CARRICULER())) {
			HashMap<String, Integer> attHashMap = hashMapKeys.get(nominalKeys
					.getKEY_DID_EXTRA_CARRICULER());
			if (attHashMap.containsKey(example.getDidExtraCarriculer())) {
				int num = attHashMap.get(example.getDidExtraCarriculer());
				attHashMap.put(example.getDidExtraCarriculer(), num + 1);
			} else {
				attHashMap.put(example.getDidExtraCarriculer(), 1);
			}
		} else {
			HashMap<String, Integer> attHashMap = new HashMap<String, Integer>();
			attHashMap.put(example.getDidExtraCarriculer(), 1);
			hashMapKeys.put(nominalKeys.getKEY_DID_EXTRA_CARRICULER(),
					attHashMap);

		}
		// set good friend in dept

		if (hashMapKeys.containsKey(nominalKeys.getKEY_HAD_FRIEND_IN_DEPT())) {
			HashMap<String, Integer> attHashMap = hashMapKeys.get(nominalKeys
					.getKEY_HAD_FRIEND_IN_DEPT());
			if (attHashMap.containsKey(example.getCloseFriendInDept())) {
				int num = attHashMap.get(example.getCloseFriendInDept());
				attHashMap.put(example.getCloseFriendInDept(), num + 1);
			} else {
				attHashMap.put(example.getCloseFriendInDept(), 1);
			}
		} else {
			HashMap<String, Integer> attHashMap = new HashMap<String, Integer>();
			attHashMap.put(example.getCloseFriendInDept(), 1);
			hashMapKeys
					.put(nominalKeys.getKEY_HAD_FRIEND_IN_DEPT(), attHashMap);

		}

	}

	private double getDistance(Sample cluster, Sample example) {
		double dis_count = 0;
		double feature_count = 0;
		if (!cluster.getGender().equals(example.getGender())) {
			feature_count++;
			dis_count++;
		}
		if (!cluster.getBatch().equals(example.getBatch())) {
			feature_count++;
			dis_count++;
		}
		if (!cluster.getResidence().equals(example.getResidence())) {
			feature_count++;
			dis_count++;
		}
		if (!cluster.getDidTuition().equals(example.getDidTuition())) {
			feature_count++;
			dis_count++;
		}
		if (!cluster.getDidExtraCarriculer().equals(
				example.getDidExtraCarriculer())) {
			feature_count++;
			dis_count++;
		}
		if (cluster.getCgpa() != example.getCgpa()) {

			double val = Math.abs(cluster.getCgpa() - example.getCgpa());
			double diff = maxList.get(INDEX_IN_MAX_MIN_LIST_CGPA)
					- minList.get(INDEX_IN_MAX_MIN_LIST_CGPA);
			if (diff != 0) {
				feature_count++;
				val = val / diff;
				dis_count += val;
			}
		}
		if (cluster.getTuitionTime() != example.getTuitionTime()) {

			double val = Math.abs(cluster.getTuitionTime()
					- example.getTuitionTime());
			double diff = maxList.get(INDEX_IN_MAX_MIN_LIST_TUITIONTIME)
					- minList.get(INDEX_IN_MAX_MIN_LIST_TUITIONTIME);
			if (diff != 0) {
				feature_count++;
				val = val / diff;
				dis_count += val;
			}
		}
		if (cluster.getExtraCarriculerTime() != example
				.getExtraCarriculerTime()) {

			double val = Math.abs(cluster.getExtraCarriculerTime()
					- example.getExtraCarriculerTime());
			double diff = maxList.get(INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME)
					- minList.get(INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME);
			if (diff != 0) {
				feature_count++;
				val = val / diff;
				dis_count += val;
			}
		}
		double dis = dis_count / feature_count;

		return dis;

	}

	private void normalizeOrdinalAttributes(String att, int attIndex,
			int sampleIndex) {

		if (attIndex == 8) {
			double val = (Double.parseDouble(att) - CGPA_MEAN)
					/ CGPA_STD_DEVIATIONS;
			sampleList.get(sampleIndex).setCgpa(val);
			if (val > maxList.get(INDEX_IN_MAX_MIN_LIST_CGPA))
				maxList.set(INDEX_IN_MAX_MIN_LIST_CGPA, val);
			if (val < minList.get(INDEX_IN_MAX_MIN_LIST_CGPA))
				minList.set(INDEX_IN_MAX_MIN_LIST_CGPA, val);

		} else

		if (attIndex == 11) {
			int size = tuitionTimeMap.size();

			int val = tuitionTimeMap.get(att);
			double v_value = (double) val / size;
			sampleList.get(sampleIndex).setTuitionTime(v_value);

			if (v_value > maxList.get(INDEX_IN_MAX_MIN_LIST_TUITIONTIME))
				maxList.set(INDEX_IN_MAX_MIN_LIST_TUITIONTIME, v_value);
			if (v_value < minList.get(INDEX_IN_MAX_MIN_LIST_TUITIONTIME))
				minList.set(INDEX_IN_MAX_MIN_LIST_TUITIONTIME, v_value);

		} else if (attIndex == 14) {
			int size = coCarriculerTimeMap.size() - 1; // -1 becuase theres a
														// empty
														// string which we
														// shoudnt
														// count;

			// System.out.println("err" + size + " att " + att);
			int val = coCarriculerTimeMap.get(att);
			double v_value = (double) val / size;
			sampleList.get(sampleIndex).setExtraCarriculerTime(v_value);

			if (v_value > maxList.get(INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME))
				maxList.set(INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME, v_value);
			if (v_value < minList.get(INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME))
				minList.set(INDEX_IN_MAX_MIN_LIST_CO_CARRICULER_TIME, v_value);

		} else if (attIndex == 1) {
			sampleList.get(sampleIndex).setGender(att);
		} else if (attIndex == 2) {
			sampleList.get(sampleIndex).setBatch(att);
		} else if (attIndex == 4) {
			sampleList.get(sampleIndex).setResidence(att);
		} else if (attIndex == 10) {
			sampleList.get(sampleIndex).setDidTuition(att);
		} else if (attIndex == 13) {
			sampleList.get(sampleIndex).setDidExtraCarriculer(att);
		} else if (attIndex == 21) {
			sampleList.get(sampleIndex).setCloseFriendInDept(att);
		}

	}

	private void generateInitialClusterCenters() {
		Random random = new Random();
		ArrayList<Integer> list = new ArrayList<Integer>();
		int count = 0;
		while (count != NUM_OF_CLUSTERS) {
			int index = random.nextInt() % TOTAL_COUNT;
			if (index < 0)
				index *= -1;

			if (!list.contains(index)) {
				count++;
				list.add(index);
			}

		}
		for (int i = 0; i < NUM_OF_CLUSTERS; i++) {
			clustList.add(sampleList.get(list.get(i)));
		}
	}

	private int getAttType(int index) {
		if (index == 1 || index == 2 || index == 4 || index == 10
				|| index == 13 || index == 21)
			return Constants.TYPE_NOMINAL;
		if (index == 11 || index == 14)
			return Constants.TYPE_ORDINAL;
		if (index == 8)
			return Constants.TYPE_NUMERIC;

		return -1;
	}

}

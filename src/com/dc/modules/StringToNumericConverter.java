package com.dc.modules;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.dc.database.DatabaseHelper;
import com.dc.utility.Constants;

public class StringToNumericConverter {

	private static final String COMMA_DELIMITER = ",";

	private static final String NEW_LINE_SEPARATOR = "\n";
	private FileWriter fileWriter = null;
	private String clean_file_name = "neumeric_data.csv";
	private final int GENDER_INDEX = 1;
	private final int BATCH_INDEX = 2;
	private final int DEPARTMENT_INDEX = 3;
	private final int RESIDENCE_INDEX = 4;
	private final int HALL_INDEX = 5;
	private final int COMMNICATION_TIME_INDEX = 6;
	private final int HOW_HALL_LIFE_EFFECT_INDEX = 7;
	private final int CGPA_INDEX = 8;
	private final int CGPA_TREND_INDEX = 9;
	private final int TUITION_ATT_INDEX = 10;
	private final int TUITION_TIME_INDEX = 11;
	private final int TUITION_REASONS_INDEX = 12;
	private final int CO_CARRICULER_INDEX = 13;
	private final int CO_CARRICULER_TIME_INDEX = 14;
	private final int CO_CARRICULER_TYPE_INDEX = 15;
	private final int STUDY_COMPARISON_INDEX = 16;
	private final int GOOD_RESULT_REASONS_INDEX = 17;
	private final int BAD_RESULT_REASONS_INDEX = 18;
	private final int AREA_NEED_TO_BE_CHANGED = 19;
	private final int MOST_TIME_SPEND_STYLE_INDEX = 20;
	private final int ClOSE_FRIEND_IN_DEPARTMENT_INDEX = 21;
	private final int MOST_TIME_SPEND_PEOPLE_INDEX = 22;
	private final int INVOLVED_IN_POLITICS_INDEX = 23;
	private final int POLITICS_EFFECT_INDEX = 24;

	private ArrayList<ArrayList<String>> attList = new ArrayList<ArrayList<String>>();

	private boolean isValid = true;

	// Hash maps

	private HashMap<String, String> genderMap = new HashMap<String, String>();
	private HashMap<String, String> batchMap = new HashMap<String, String>();
	private HashMap<String, String> departmentMap = new HashMap<String, String>();
	private HashMap<String, String> residenseMap = new HashMap<String, String>();
	private HashMap<String, String> hallNameMap = new HashMap<String, String>();
	private HashMap<String, String> CommunicationTimeMap = new HashMap<String, String>();
	private HashMap<String, String> hallLifeEffectMap = new HashMap<String, String>();
	private HashMap<String, String> cgpaMap = new HashMap<String, String>();
	private HashMap<String, String> cgpaTrendMap = new HashMap<String, String>();
	private HashMap<String, String> didTuitionMap = new HashMap<String, String>();
	private HashMap<String, String> tuitionTimeMap = new HashMap<String, String>();
	private HashMap<String, String> tuitionReasonsMap = new HashMap<String, String>();
	private HashMap<String, String> didCocaariculerMap = new HashMap<String, String>();
	private HashMap<String, String> coCarriculerTimeMap = new HashMap<String, String>();
	private HashMap<String, String> coCarriculerTypeMap = new HashMap<String, String>();
	private HashMap<String, String> studyComparisonMap = new HashMap<String, String>();
	private HashMap<String, String> goodResultReasonsMap = new HashMap<String, String>();
	private HashMap<String, String> badResultReasonsMap = new HashMap<String, String>();
	private HashMap<String, String> areaNeedsToBeChangedMap = new HashMap<String, String>();
	private HashMap<String, String> mostTimeSpendStyleMap = new HashMap<String, String>();
	private HashMap<String, String> closeFriendInDeptMap = new HashMap<String, String>();
	private HashMap<String, String> mostTimeSpendWithPeopleMap = new HashMap<String, String>();
	private HashMap<String, String> invlovedInPoliticsMap = new HashMap<String, String>();

	public StringToNumericConverter() {
		createArrayList();
		openFile();
		readRawFile();
		closeFile();
	}

	private void createArrayList() {
		for (int i = 0; i < Constants.COLUMN_NO; i++) {
			ArrayList<String> list = new ArrayList<String>();
			attList.add(list);
		}
	}

	private void readRawFile() {
		int count = 0;
		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader("clean_data_2.csv"));
			while ((sCurrentLine = br.readLine()) != null) {

				String[] values = sCurrentLine.split(",", -1);
				// System.out.println(""+values.length+"  "+ sCurrentLine);

				count++;

				isValid = true;
				values = convertGenderAttribute(values, count);
				values = convertBatchAttribute(values, count);
				values = convertDepartmentAttribute(values, count);
				values = convertResidentAttribute(values, count);
				values = convertHallNameAttribute(values, count);
				values = convertHomeeToBuetAttribute(values, count);
				values = convertHallEffetsAttribute(values, count);
				values = convertCGPAttribute(values, count);
				values = convertResultChangeAttribute(values, count);
				values = convertDidTuitionAttribute(values, count);
				values = convertTuitionTimeAttribute(values, count);
				values = convertTuitionReasonsAttribute(values, count);
				values = convertDidCoCarriculerAttribute(values, count);
				values = convertCoCarriculerTimeAttribute(values, count);
				values = convertCoCarriculerTypeAttribute(values, count);
				values = convertStudyComparisonAttribute(values, count);
				values = convertGoodResultReasonsAttribute(values, count);
				values = convertBadResultReasonsAttribute(values, count);
				values = convertThingsNeedToBeChangedAttribute(values, count);
				values = convertMostTimeSpendAttribute(values, count);
				values = convertCloseFriendInDeptAttribute(values, count);
				values = convertMostTimeSpendWithPeopleAttribute(values, count);
				values = convertInvlovedInPoliticsAttribute(values, count);

				// values[0]="";
				// moving cgpa to last Index

				// String temp = values[CGPA_INDEX];
				// values[CGPA_INDEX] = values[INVOLVED_IN_POLITICS_INDEX];
				// values[INVOLVED_IN_POLITICS_INDEX] = temp;
				String row = "";
				if (isValid) {
					for (int j = 0; j < Constants.COLUMN_NO; j++) {
						String att = convertToCleanAttribute(values[j]);
						if (j == Constants.COLUMN_NO - 1)
							row = row + att + NEW_LINE_SEPARATOR;
						else
							row = row + att + COMMA_DELIMITER;
					}
					System.out.println("rowIndex: " + count + "   " + row);
					saveRow(row);
				}

				// if (count > 100)
				// break;
				// creating maps
				// System.out.println("instanc: "+ count);

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		System.out.println("TOTAL INSTANCES: " + count);
		// printAttributes();
		// saveAttributes();
	}

	private String[] convertGenderAttribute(String[] values, int rowIndex) {

		if (values[GENDER_INDEX].equals("Male"))
			values[GENDER_INDEX] = "1";
		else if (values[GENDER_INDEX].equals("Female"))
			values[GENDER_INDEX] = "0";
		return values;
	}

	private String[] convertBatchAttribute(String[] values, int rowIndex) {

		int size = batchMap.size();
		if (batchMap.containsKey(values[BATCH_INDEX])) {
			values[BATCH_INDEX] = "" + batchMap.get(values[BATCH_INDEX]);
		} else {
			batchMap.put(values[BATCH_INDEX], "" + size);
			values[BATCH_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertDepartmentAttribute(String[] values, int rowIndex) {
		int size = departmentMap.size();
		if (departmentMap.containsKey(values[DEPARTMENT_INDEX])) {
			values[DEPARTMENT_INDEX] = ""
					+ departmentMap.get(values[DEPARTMENT_INDEX]);
		} else {
			departmentMap.put(values[DEPARTMENT_INDEX], "" + size);
			values[DEPARTMENT_INDEX] = "" + size;

		}
		return values;

	}

	private String[] convertResidentAttribute(String[] values, int rowIndex) {
		int size = residenseMap.size();
		if (residenseMap.containsKey(values[RESIDENCE_INDEX])) {
			values[RESIDENCE_INDEX] = ""
					+ residenseMap.get(values[RESIDENCE_INDEX]);
		} else {
			residenseMap.put(values[RESIDENCE_INDEX], "" + size);
			values[RESIDENCE_INDEX] = "" + size;

		}

		return values;
	}

	private String[] convertHallNameAttribute(String[] values, int rowIndex) {

		int size = hallNameMap.size();
		if (hallNameMap.containsKey(values[HALL_INDEX])) {
			values[HALL_INDEX] = "" + hallNameMap.get(values[HALL_INDEX]);
		} else {
			hallNameMap.put(values[HALL_INDEX], "" + size);
			values[HALL_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertHomeeToBuetAttribute(String[] values, int rowIndex) {

		// no change now
		int d = Integer.parseInt(values[COMMNICATION_TIME_INDEX].replaceAll(
				" ", ""));
		if (d < 10) {
			values[COMMNICATION_TIME_INDEX] = "" + 5;
		} else if (d < 20) {
			values[COMMNICATION_TIME_INDEX] = "" + 15;
		} else if (d < 30) {
			values[COMMNICATION_TIME_INDEX] = "" + 25;
		} else if (d < 40) {
			values[COMMNICATION_TIME_INDEX] = "" + 35;
		} else if (d < 50) {
			values[COMMNICATION_TIME_INDEX] = "" + 45;
		} else if (d < 60) {
			values[COMMNICATION_TIME_INDEX] = "" + 55;
		} else if (d < 70) {
			values[COMMNICATION_TIME_INDEX] = "" + 65;
		} else if (d < 80) {
			values[COMMNICATION_TIME_INDEX] = "" + 75;
		} else if (d < 90) {
			values[COMMNICATION_TIME_INDEX] = "" + 85;
		} else if (d < 100) {
			values[COMMNICATION_TIME_INDEX] = "" + 95;
		} else if (d < 120) {
			values[COMMNICATION_TIME_INDEX] = "" + 105;
		} else
			values[COMMNICATION_TIME_INDEX] = "" + 115;

		return values;
	}

	private String[] convertHallEffetsAttribute(String[] values, int rowIndex) {

		int size = hallLifeEffectMap.size();
		if (hallLifeEffectMap.containsKey(values[HOW_HALL_LIFE_EFFECT_INDEX])) {
			values[HOW_HALL_LIFE_EFFECT_INDEX] = ""
					+ hallLifeEffectMap.get(values[HOW_HALL_LIFE_EFFECT_INDEX]);
		} else {
			hallLifeEffectMap
					.put(values[HOW_HALL_LIFE_EFFECT_INDEX], "" + size);
			values[HOW_HALL_LIFE_EFFECT_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertCGPAttribute(String[] values, int rowIndex) {

		Double cg = Double.parseDouble(values[CGPA_INDEX]);
		if (cg > 3.33 && cg < 3.38)
			isValid=false;
		else if(cg>=3.38)
			values[CGPA_INDEX] = "" + 1;
		else
			values[CGPA_INDEX] = "" + 0;

		return values;
	}

	private String[] convertResultChangeAttribute(String[] values, int rowIndex) {

		int size = cgpaTrendMap.size();
		if (cgpaTrendMap.containsKey(values[CGPA_TREND_INDEX])) {
			values[CGPA_TREND_INDEX] = ""
					+ cgpaTrendMap.get(values[CGPA_TREND_INDEX]);
		} else {
			cgpaTrendMap.put(values[CGPA_TREND_INDEX], "" + size);
			values[CGPA_TREND_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertDidTuitionAttribute(String[] values, int rowIndex) {

		if (values[TUITION_ATT_INDEX].equals("Yes"))
			values[TUITION_ATT_INDEX] = "1";
		else
			values[TUITION_ATT_INDEX] = "0";

		return values;
	}

	private String[] convertTuitionTimeAttribute(String[] values, int rowIndex) {
		int size = tuitionTimeMap.size();
		if (tuitionTimeMap.containsKey(values[TUITION_TIME_INDEX])) {
			values[TUITION_TIME_INDEX] = ""
					+ tuitionTimeMap.get(values[TUITION_TIME_INDEX]);
		} else {
			tuitionTimeMap.put(values[TUITION_TIME_INDEX], "" + size);
			values[TUITION_TIME_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertTuitionReasonsAttribute(String[] values,
			int rowIndex) {
		int size = tuitionReasonsMap.size();
		if (tuitionReasonsMap.containsKey(values[TUITION_REASONS_INDEX])) {
			values[TUITION_REASONS_INDEX] = ""
					+ tuitionReasonsMap.get(values[TUITION_REASONS_INDEX]);
		} else {
			tuitionReasonsMap.put(values[TUITION_REASONS_INDEX], "" + size);
			values[TUITION_REASONS_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertDidCoCarriculerAttribute(String[] values,
			int rowIndex) {
		if (values[CO_CARRICULER_INDEX].contains("Yes"))
			values[CO_CARRICULER_INDEX] = "" + 1;
		else
			values[CO_CARRICULER_INDEX] = "" + 0;
		return values;
	}

	private String[] convertCoCarriculerTimeAttribute(String[] values,
			int rowIndex) {
		int size = coCarriculerTimeMap.size();
		if (coCarriculerTimeMap.containsKey(values[CO_CARRICULER_TIME_INDEX])) {
			values[CO_CARRICULER_TIME_INDEX] = ""
					+ coCarriculerTimeMap.get(values[CO_CARRICULER_TIME_INDEX]);
		} else {
			coCarriculerTimeMap
					.put(values[CO_CARRICULER_TIME_INDEX], "" + size);
			values[CO_CARRICULER_TIME_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertCoCarriculerTypeAttribute(String[] values,
			int rowIndex) {
		int size = coCarriculerTypeMap.size();
		if (coCarriculerTypeMap.containsKey(values[CO_CARRICULER_TYPE_INDEX])) {
			values[CO_CARRICULER_TYPE_INDEX] = ""
					+ coCarriculerTypeMap.get(values[CO_CARRICULER_TYPE_INDEX]);
		} else {
			coCarriculerTypeMap
					.put(values[CO_CARRICULER_TYPE_INDEX], "" + size);
			values[CO_CARRICULER_TYPE_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertStudyComparisonAttribute(String[] values,
			int rowIndex) {

		int size = studyComparisonMap.size();
		if (studyComparisonMap.containsKey(values[STUDY_COMPARISON_INDEX])) {
			values[STUDY_COMPARISON_INDEX] = ""
					+ studyComparisonMap.get(values[STUDY_COMPARISON_INDEX]);
		} else {
			studyComparisonMap.put(values[STUDY_COMPARISON_INDEX], "" + size);
			values[STUDY_COMPARISON_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertGoodResultReasonsAttribute(String[] values,
			int rowIndex) {

		int size = goodResultReasonsMap.size();
		if (goodResultReasonsMap.containsKey(values[GOOD_RESULT_REASONS_INDEX])) {
			values[GOOD_RESULT_REASONS_INDEX] = ""
					+ goodResultReasonsMap
							.get(values[GOOD_RESULT_REASONS_INDEX]);
		} else {
			goodResultReasonsMap.put(values[GOOD_RESULT_REASONS_INDEX], ""
					+ size);
			values[GOOD_RESULT_REASONS_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertBadResultReasonsAttribute(String[] values,
			int rowIndex) {
		int size = badResultReasonsMap.size();
		if (badResultReasonsMap.containsKey(values[BAD_RESULT_REASONS_INDEX])) {
			values[BAD_RESULT_REASONS_INDEX] = ""
					+ badResultReasonsMap.get(values[BAD_RESULT_REASONS_INDEX]);
		} else {
			badResultReasonsMap
					.put(values[BAD_RESULT_REASONS_INDEX], "" + size);
			values[BAD_RESULT_REASONS_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertThingsNeedToBeChangedAttribute(String[] values,
			int rowIndex) {
		// System.out.println(values[AREA_NEED_TO_BE_CHANGED]);
		int size = areaNeedsToBeChangedMap.size();
		if (areaNeedsToBeChangedMap
				.containsKey(values[AREA_NEED_TO_BE_CHANGED])) {
			values[AREA_NEED_TO_BE_CHANGED] = ""
					+ areaNeedsToBeChangedMap
							.get(values[AREA_NEED_TO_BE_CHANGED]);

		} else {
			areaNeedsToBeChangedMap.put(values[AREA_NEED_TO_BE_CHANGED], ""
					+ size);
			values[AREA_NEED_TO_BE_CHANGED] = "" + size;

		}
		return values;
	}

	private String[] convertMostTimeSpendAttribute(String[] values, int rowIndex) {

		int size = mostTimeSpendStyleMap.size();
		if (mostTimeSpendStyleMap
				.containsKey(values[MOST_TIME_SPEND_STYLE_INDEX])) {
			values[MOST_TIME_SPEND_STYLE_INDEX] = ""
					+ mostTimeSpendStyleMap
							.get(values[MOST_TIME_SPEND_STYLE_INDEX]);

		} else {
			mostTimeSpendStyleMap.put(values[MOST_TIME_SPEND_STYLE_INDEX], ""
					+ size);
			values[MOST_TIME_SPEND_STYLE_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertCloseFriendInDeptAttribute(String[] values,
			int rowIndex) {

		if (values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX].contains("Yes"))
			values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX] = "1";
		else
			values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX] = "0";
		return values;
	}

	private String[] convertMostTimeSpendWithPeopleAttribute(String[] values,
			int rowIndex) {
		int size = mostTimeSpendWithPeopleMap.size();
		if (mostTimeSpendWithPeopleMap
				.containsKey(values[MOST_TIME_SPEND_PEOPLE_INDEX])) {
			values[MOST_TIME_SPEND_PEOPLE_INDEX] = ""
					+ mostTimeSpendWithPeopleMap
							.get(values[MOST_TIME_SPEND_PEOPLE_INDEX]);

		} else {
			mostTimeSpendWithPeopleMap.put(
					values[MOST_TIME_SPEND_PEOPLE_INDEX], "" + size);
			values[MOST_TIME_SPEND_PEOPLE_INDEX] = "" + size;

		}
		return values;
	}

	private String[] convertInvlovedInPoliticsAttribute(String[] values,
			int rowIndex) {

		if (values[INVOLVED_IN_POLITICS_INDEX].contains("Yes"))
			values[INVOLVED_IN_POLITICS_INDEX] = "" + 1;
		else
			values[INVOLVED_IN_POLITICS_INDEX] = "0";
		return values;
	}

	private void openFile() {
		try {
			fileWriter = new FileWriter(clean_file_name);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void closeFile() {
		try {
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void saveRow(String row) {
		// System.out.println(row);
		try {
			fileWriter.append(row);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printAttributes() {
		for (int i = 0; i < Constants.COLUMN_NO; i++) {
			System.out.println("Attribute " + i + " Size: "
					+ attList.get(i).size());
			if (i == 1) {
				for (int j = 0; j < attList.get(i).size(); j++) {
					System.out.println(attList.get(i).get(j));
				}
				System.out.println("\n\n");
			}
		}
	}

	private void addAttributeinList(String input, int index) {
		if (input.length() > 0) {
			if (!attList.get(index).contains(input))
				attList.get(index).add(input);
		}

	}

	private String convertToCleanAttribute(String att) {
		String str = att.replaceAll("\"", ""); // replacing all " letters
		str = str.replaceAll("\'", ""); // replacing all \' charcaters
		str = str.replaceAll(",", ":"); // replacing all , charcter with :
		// System.out.println(str);
		return str;
	}

	private void saveAttributes() {
		DatabaseHelper dbHelper = new DatabaseHelper();
		for (int i = 0; i < Constants.COLUMN_NO; i++)
			dbHelper.insertDataIntoDatabase(i, attList.get(i));

	}
}

package com.dc.modules;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.dc.model.Sample;
import com.dc.model.SampleRaw;

public class DecessionTree {

	private ArrayList<SampleRaw> sampleList = new ArrayList<SampleRaw>();
	private ArrayList<Integer> targetAttList = new ArrayList<Integer>();
	private ArrayList<String> selectedAttList = new ArrayList<String>();
	private ArrayList<ArrayList<String>> attValueList=new ArrayList<ArrayList<String>>();
	int count = 0;

	public DecessionTree() {

		readRawFileForParsing();
	}

	private void readRawFileForParsing() {
		count = 0;
		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader("clean_data.csv"));
			while ((sCurrentLine = br.readLine()) != null) {
				if (count != 0) {
					String[] values = sCurrentLine.split(",", -1);
					SampleRaw sample = new SampleRaw();
					sampleList.add(sample);
					for (int i = 0; i < values.length; i++)
						loadAttributes(values[i], i, count - 1);
					// System.out.println("" + values.length);

				}
				count++;
				if (count > 400)
					break;

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		// printSamples();
		// printMaxMins();

	}

	private void loadAttributes(String att, int attIndex, int sampleIndex) {

		if (attIndex == 8) {

			Double cgpa = Double.parseDouble(att);
			int c=0;
			if (cgpa >= 2.0 && cgpa < 2.25)
				c = 1;
			else if (cgpa >= 2.25 && cgpa < 2.5)
				c = 2;
			else if(cgpa>=2.5 && cgpa<2.75)
				c=3;
			else if(cgpa>=2.75 && cgpa<3.00)
				c=4;
			else if (cgpa >= 3.0 && cgpa < 3.25)
				c = 5;
			else if (cgpa >= 3.25 && cgpa < 3.5)
				c = 6;
			else if(cgpa>=3.5 && cgpa<3.75)
				c=3;
			else if(cgpa>=3 && cgpa<=4.00)
				c=7;
			targetAttList.set(sampleIndex, c);
			sampleList.get(sampleIndex).setCgpa(c);
		} else if (attIndex == 11) {
				if(att.length()==0)
					att="N/A";
				sampleList.get(sampleIndex).setTuitionTime(att);
		} else if (attIndex == 14) {
			if(att.length()==0)
				att="N/A";
			sampleList.get(sampleIndex).setExtraCarriculerTime(att);
		} else if (attIndex == 1) {
			sampleList.get(sampleIndex).setGender(att);
		} else if (attIndex == 2) {
			sampleList.get(sampleIndex).setBatch(att);
		} else if (attIndex == 4) {
			sampleList.get(sampleIndex).setResidence(att);
		} else if (attIndex == 10) {
			sampleList.get(sampleIndex).setDidTuition(att);
		} else if (attIndex == 13) {
			sampleList.get(sampleIndex).setDidExtraCarriculer(att);
		} else if (attIndex == 21) {
			sampleList.get(sampleIndex).setCloseFriendInDept(att);
		}

	}

}

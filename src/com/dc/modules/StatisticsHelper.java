package com.dc.modules;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class StatisticsHelper {

	private double CGPA_SUM = 0;
	private double CGPA_MEAN=0;
	private double STD_SUM = 0;
	private double STD_DEVIATIONS=0;
	int count=0;

	public StatisticsHelper() {
		readRawFileForSum();
		readRawFileForStdDeviations();
		writeinFile();
	}

	private void readRawFileForSum() {
		count = 0;
		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader("clean_data.csv"));
			while ((sCurrentLine = br.readLine()) != null) {
				if (count != 0) {
					String[] values = sCurrentLine.split(",", -1);

					//System.out.println("" + values.length);
					sum_CGPA(values[8]);
				}
				count++;
				if (count > 200)
					break;

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		meanCGPA();

	}
	private void readRawFileForStdDeviations() {
		count = 0;
		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader("clean_data.csv"));
			while ((sCurrentLine = br.readLine()) != null) {
				if (count != 0) {
					String[] values = sCurrentLine.split(",", -1);

					//System.out.println("" + values.length);
					sum_std_deciations(values[8]);
				}
				count++;
				if (count > 200)
					break;

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		calcStdDeviations();

	}

	private void sum_CGPA(String cg) {
		CGPA_SUM += Double.parseDouble(cg);
	}
	private void sum_std_deciations(String cg) {
		STD_SUM += Math.pow(Double.parseDouble(cg)-CGPA_MEAN,2);
	}
	private void meanCGPA()
	{
		CGPA_MEAN=(CGPA_SUM/(count-1));
	  System.out.println("CGPA_MAEN: "+CGPA_MEAN);
		
	}
	private void calcStdDeviations()
	{
		STD_DEVIATIONS=Math.sqrt(STD_SUM/(count-1));
		System.out.println("STD_DEV: "+STD_DEVIATIONS);
		
	}
	private void writeinFile()
	{
		try {

			String content = "8:mean:"+CGPA_MEAN+":stdev:"+STD_DEVIATIONS;

			File file = new File("stats.txt");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

package com.dc.modules;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import com.dc.database.DatabaseHelper;
import com.dc.utility.Constants;

public class DataCleaner {

	private static final String COMMA_DELIMITER = ",";

	private static final String NEW_LINE_SEPARATOR = "\n";
	private FileWriter fileWriter = null;
	private String clean_file_name = "clean_data_2.csv";
	private final int GENDER_INDEX = 1;
	private final int BATCH_INDEX = 2;
	private final int DEPARTMENT_INDEX = 3;
	private final int RESIDENCE_INDEX = 4;
	private final int HALL_INDEX = 5;
	private final int COMMNICATION_TIME_INDEX = 6;
	private final int HOW_HALL_LIFE_EFFECT_INDEX = 7;
	private final int CGPA_INDEX = 8;
	private final int CGPA_TREND_INDEX = 9;
	private final int TUITION_ATT_INDEX = 10;
	private final int TUITION_TIME_INDEX = 11;
	private final int TUITION_REASONS_INDEX = 12;
	private final int CO_CARRICULER_INDEX = 13;
	private final int CO_CARRICULER_TIME_INDEX = 14;
	private final int EXTRA_CARRICULER_TYPE_INDEX = 15;
	private final int STUDY_COMPARISON_INDEX = 16;
	private final int GOOD_RESULT_REASONS_INDEX = 17;
	private final int BAD_RESULT_REASONS_INDEX = 18;
	private final int AREA_NEED_TO_BE_CHANGED = 19;
	private final int MOST_TIME_SPEND_STYLE_INDEX = 20;
	private final int ClOSE_FRIEND_IN_DEPARTMENT_INDEX = 21;
	private final int MOST_TIME_SPEND_PEOPLE_INDEX = 22;
	private final int INVOLVED_IN_POLITICS_INDEX = 23;
	private final int POLITICS_EFFECT_INDEX = 24;

	private final int MOST_TIME_WITH_INDEX = 0;

	private Random rand = new Random();

	private ArrayList<ArrayList<String>> attList = new ArrayList<ArrayList<String>>();

	public DataCleaner() {
		createArrayList();
		openFile();
		readRawFile();
		closeFile();
	}

	private void createArrayList() {
		for (int i = 0; i < Constants.COLUMN_NO; i++) {
			ArrayList<String> list = new ArrayList<String>();
			attList.add(list);
		}
	}

	private void readRawFile() {
		int count = 0;
		BufferedReader br = null;
		try {

			String sCurrentLine;
			String totalLine = "";
			br = new BufferedReader(new FileReader("buet_data.csv"));
			while ((sCurrentLine = br.readLine()) != null) {

				String[] values = sCurrentLine.split("\",");
				// System.out.println(""+values.length);
				if (values.length < Constants.COLUMN_NO) {
					// System.out.println(sCurrentLine);
					totalLine = totalLine + sCurrentLine;

				} else
					totalLine = sCurrentLine;

				values = totalLine.split("\",");
				if (values.length >= Constants.COLUMN_NO) {
					sCurrentLine = sCurrentLine.replaceAll("\n", "");
					values = totalLine.split("\",");
					count++;

					if (count > 1)

					{
						// System.out.println(sCurrentLine);
						values = checkReidenceConsistency(values, count);
						values = convertHallLifeEffectAtt(values, count);
						values = checkTuitionAttConsistency(values, count);
						values = convertTuitionReasons(values, count);
						values = checkCoCarriculerAttConsistency(values, count);
						values = convertCoCarriculerTypeAtt(values, count);
						values = convertGoodResultsReasons(values, count);
						values = convertBadResultReasons(values, count);
						values = convertAreaNeedsToBeChangedAtt(values, count);
						values = convertMostTimeSpendAtt(values, count);
						values = convertCloseFriendInDept(values, count);
						values = convertMostTimeSpendPeople(values, count);
						values = convertInvolvedInPolitics(values, count);

						values[0]="";
						values[24] = "";
						values[25] = "";
						values[26] = "";
						// System.out.println("T "+values.length);
						String row = "";
						for (int j = 0; j < Constants.COLUMN_NO; j++) {
							String att = convertToCleanAttribute(values[j]);
							if (j == Constants.COLUMN_NO - 1)
								row = row + att + NEW_LINE_SEPARATOR;
							else
								row = row + att + COMMA_DELIMITER;
						}
						// System.out.println("count: " + count + "  " + row);
						saveRow(row);
						totalLine = "";
					}
					// if(count>2)
					// break;
					// creating maps
					// System.out.println("instanc: "+ count);

				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		System.out.println("TOTAL INSTANCES: " + count);
		// printAttributes();
		// saveAttributes();
	}

	private String[] checkReidenceConsistency(String[] values, int rowIndex) {

		if (values[RESIDENCE_INDEX].contains("Home sweet Home")) {
			values[HALL_INDEX] = "N/A";

			// System.out.println("hello 1");
			if (values[COMMNICATION_TIME_INDEX].length() == 1) {
				// System.out.println("Incosistency in hall attributes No"
				// + " in " + rowIndex);
				values[COMMNICATION_TIME_INDEX] = "" + 0;
			}
		} else if (values[RESIDENCE_INDEX].contains("Hall(Home in Dhaka)")) {
			if (values[HALL_INDEX].contains("Nahar"))
				values[HALL_INDEX] = "Chatri Hall";
			if (values[HALL_INDEX].equals("N/A")
					|| values[HALL_INDEX].length() == 1) {
				// System.out.println("Incosistency in hall attributes no hall No"
				// + " in " + rowIndex);
			}
			if (values[COMMNICATION_TIME_INDEX].length() != 0) {
				// System.out.println("Incosistency in hall time given No"
				// + " in " + rowIndex);
			}
			values[COMMNICATION_TIME_INDEX] = "" + 0;
			if (values[HALL_INDEX].length() == 1) {
				if (values[GENDER_INDEX].contains("Female"))
					values[HALL_INDEX] = "Chatri Hall";
				else {
					int hall = rand.nextInt() % 7;
					if (hall < 0)
						hall *= -1;
					if (hall == 0) {
						values[HALL_INDEX] = "Ahsanullah Hall-North";
					} else if (hall == 1) {
						values[HALL_INDEX] = "Ahsanullah Hall-West";
					} else if (hall == 2) {
						values[HALL_INDEX] = "Nazrul Hall";
					} else if (hall == 3) {
						values[HALL_INDEX] = "Suhrawardy Hall";
					} else if (hall == 4) {
						values[HALL_INDEX] = "Shere-E-Bangla Hall";
					} else if (hall == 5) {
						values[HALL_INDEX] = "Rashid Hall";
					} else if (hall == 6) {
						values[HALL_INDEX] = "Titumir Hall";
					}

				}
			}

		} else if (values[RESIDENCE_INDEX].contains("Hall(Home outside Dhaka)")) {

			if (values[HALL_INDEX].contains("Nahar"))
				values[HALL_INDEX] = "Chatri Hall";
			if (values[HALL_INDEX].equals("N/A")
					|| values[HALL_INDEX].length() == 0) {
				// System.out.println("Incosistency in hall attributes no hall No"
				// + " in " + rowIndex);
			}
			if (values[COMMNICATION_TIME_INDEX].length() != 0) {
				// System.out.println("Incosistency in hall time given No"
				// + " in " + rowIndex);
			}
			values[COMMNICATION_TIME_INDEX] = "" + 0;
			if (values[HALL_INDEX].length() == 1) {

				if (values[GENDER_INDEX].contains("Female"))
					values[HALL_INDEX] = "Chatri Hall";
				else {
					int hall = rand.nextInt() % 7;
					if (hall < 0)
						hall *= -1;
					if (hall == 0) {
						values[HALL_INDEX] = "Ahsanullah Hall-North";
					} else if (hall == 1) {
						values[HALL_INDEX] = "Ahsanullah Hall-West";
					} else if (hall == 2) {
						values[HALL_INDEX] = "Nazrul Hall";
					} else if (hall == 3) {
						values[HALL_INDEX] = "Suhrawardy Hall";
					} else if (hall == 4) {
						values[HALL_INDEX] = "Shere-E-Bangla Hall";
					} else if (hall == 5) {
						values[HALL_INDEX] = "Rashid Hall";
					} else if (hall == 6) {
						values[HALL_INDEX] = "Titumir Hall";
					}

				}
			}

		}

		return values;
	}

	private String[] convertHallLifeEffectAtt(String[] values, int rowIndex) {
		String attValue = values[HOW_HALL_LIFE_EFFECT_INDEX].toLowerCase();
		String convertedValue = "";
		if (attValue.contains("problem with")
				|| attValue.contains("discouraging")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";

			convertedValue = convertedValue + "Human Factor";
		}
		if (attValue.contains("food") || attValue.contains("hygienic")
				|| attValue.contains("noisy")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Hall Environment Factor";
		}
		if (attValue.contains("home sickness")
				|| attValue.contains("privacy issue")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue += "Phsycological Factor";
		}
		if (convertedValue.length() == 0)
			convertedValue = "N/A";
		// convertedValue = "Others:";

		// System.out.println(" row: " + rowIndex + "  actual: " + attValue
		// + "  converted: " + convertedValue);
		values[HOW_HALL_LIFE_EFFECT_INDEX] = convertedValue;
		return values;
	}

	private String[] checkTuitionAttConsistency(String[] values, int rowIndex) {
		// System.out.println(values[TUITION_TIME_INDEX]+" count: "+
		// rowIndex+"  ");
		if (values[TUITION_ATT_INDEX].contains("No")) {
			if (!values[TUITION_TIME_INDEX].contains("N/A"))
				// System.out.println("Incosistency in tuition attributes No"
				// + " in " + rowIndex);
				values[TUITION_TIME_INDEX] = "N/A";
		} else if (values[TUITION_ATT_INDEX].contains("Yes")) {
			if (values[TUITION_TIME_INDEX].length() == 1)
				// System.out.println("Incosistency in tuition attributes yes"
				// + " in " + rowIndex);
				values[TUITION_TIME_INDEX] = "0";
		} else {
			// System.out.println("value: " + values[CO_CARRICULER_INDEX]);
		}
		
		if (values[TUITION_ATT_INDEX].contains("No")) {
			values[TUITION_TIME_INDEX] = "0";
		} else {
			// System.out.println(values[TUITION_TIME_INDEX]+" count: "+
			// rowIndex+"  ");
			if (values[TUITION_TIME_INDEX].contains("<=5")
					|| values[TUITION_TIME_INDEX].contains("6-10")
					|| values[TUITION_TIME_INDEX].contains("11-15")
					|| values[TUITION_TIME_INDEX].contains("16-20")
					|| values[TUITION_TIME_INDEX].contains("21-25")
					|| values[TUITION_TIME_INDEX].contains("26-30")
					|| values[TUITION_TIME_INDEX].contains("31-35")
					|| values[TUITION_TIME_INDEX].contains(">35")) {

			} else {
				int val = rand.nextInt() % 8;
				if (val < 0)
					val *= -1;

				if (val == 0) {
					values[TUITION_TIME_INDEX] = "<=5";
				} else if (val == 1) {
					values[TUITION_TIME_INDEX] = "6-10";
				} else if (val == 2) {
					values[TUITION_TIME_INDEX] = "11-15";
				} else if (val == 3) {
					values[TUITION_TIME_INDEX] = "16-20";
				} else if (val == 4) {
					values[TUITION_TIME_INDEX] = "21-25";
				} else if (val == 5) {
					values[TUITION_TIME_INDEX] = "26-30";
				} else if (val == 6) {
					values[TUITION_TIME_INDEX] = "31-35";
				} else if (val == 7) {
					values[TUITION_TIME_INDEX] = ">35";
				}

			}
		}
		return values;
	}

	private String[] convertTuitionReasons(String[] values, int rowIndex) {
		String convertedValue = "";
		if (values[TUITION_REASONS_INDEX].contains("To have own expenditure")) {

			if (convertedValue.length() != 0)
				convertedValue += ":";

			convertedValue = convertedValue + "To have own expenditure";
		}
		if (values[TUITION_REASONS_INDEX].contains("To support family")) {

			if (convertedValue.length() != 0)
				convertedValue += ":";

			convertedValue = convertedValue + "To support family";
		}
		if (values[TUITION_REASONS_INDEX].contains("Hobby")) {

			if (convertedValue.length() != 0)
				convertedValue += ":";

			convertedValue = convertedValue + "Hobby";
		}
		if(convertedValue.length()==0)
			convertedValue="N/A";
		// System.out.println(" row: " + rowIndex + "  actual: "
		// + values[TUITION_REASONS_INDEX] + "  converted: "
		// + convertedValue);
		values[TUITION_REASONS_INDEX] = convertedValue;
		return values;
	}

	private String[] convertMostTimeSpendAtt(String[] values, int rowIndex) {
		String attValue = values[MOST_TIME_SPEND_STYLE_INDEX].toLowerCase();
		String convertedValue = "";
		if (attValue.contains("study")) {
			convertedValue = "Study";
		} else if (attValue.contains("programming")
				|| attValue.contains("competion")
				|| attValue.contains("research")
				|| attValue.contains("robotics")) {

			convertedValue = "Academic Extra Carriculer";

		} else if (attValue.contains("music") || attValue.contains("sports")
				|| attValue.contains("dance")
				|| attValue.contains("organization")
				|| attValue.contains("volunteering")
				|| attValue.contains("books")) {
			convertedValue = "Co carriculer";
		} else
			convertedValue = "Not productive";
		// System.out.println(" row: " + rowIndex + "  actual: " + attValue
		// + "  converted: " + convertedValue);
		values[MOST_TIME_SPEND_STYLE_INDEX] = convertedValue;
		return values;
	}

	private String[] convertCoCarriculerTypeAtt(String[] values, int rowIndex) {
		String attValue = values[EXTRA_CARRICULER_TYPE_INDEX].toLowerCase();
		String convertedValue = "";
		if (attValue.contains("contest") || attValue.contains("reseacrh")
				|| attValue.contains("competition")
				|| attValue.contains("robotics")
				|| attValue.contains("software"))
			convertedValue = "Academic";
		else if (attValue.contains("sport") || attValue.contains("music")
				|| attValue.contains("drama") || attValue.contains("dance")
				|| attValue.contains("photography")
				|| attValue.contains("writing") || attValue.contains("work"))
			convertedValue = "Cultural";
		else
			convertedValue = "Non Productive";
		// System.out.println(" row: " + rowIndex + "  actual: " + attValue
		// + "  converted: " + convertedValue);
		values[EXTRA_CARRICULER_TYPE_INDEX] = convertedValue;
		return values;
	}

	private String[] convertGoodResultsReasons(String[] values, int rowIndex) {
		String convertedValue = "";
		if (values[GOOD_RESULT_REASONS_INDEX].contains("Family support")
				|| values[GOOD_RESULT_REASONS_INDEX]
						.contains("Support from non-family member")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Family Support";
		}
		if (values[GOOD_RESULT_REASONS_INDEX]
				.contains("Continuation of previous results")
				|| values[GOOD_RESULT_REASONS_INDEX]
						.contains("Mostly own effort")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Own Effort";
		}
		if (values[GOOD_RESULT_REASONS_INDEX]
				.contains("Meeting good departmental friends")
				|| values[GOOD_RESULT_REASONS_INDEX]
						.contains("Meeting good non-departmental friends")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Friend";
		}
		if (convertedValue.length() == 0)
			convertedValue = "Others";

		values[GOOD_RESULT_REASONS_INDEX] = convertedValue;
		return values;
	}

	private String[] convertBadResultReasons(String[] values, int rowIndex) {
		String convertedValue = "";
		if (values[BAD_RESULT_REASONS_INDEX].contains("dissapointed")
				|| values[BAD_RESULT_REASONS_INDEX].contains("dipression")
				|| values[BAD_RESULT_REASONS_INDEX].contains("Procast")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Dissapointment";
		}
		if (values[BAD_RESULT_REASONS_INDEX].contains("wrong department")
				|| values[BAD_RESULT_REASONS_INDEX].contains("cope with")
				|| values[BAD_RESULT_REASONS_INDEX].contains("CGPA does")
				|| values[BAD_RESULT_REASONS_INDEX].contains("enough in life")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Lack of Guidence";
		}
		if (values[BAD_RESULT_REASONS_INDEX].contains("problem")
				|| values[BAD_RESULT_REASONS_INDEX].contains("circle")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Family Problem";
		}
		if (convertedValue.length() == 0)
			convertedValue = "Others";
		values[BAD_RESULT_REASONS_INDEX] = convertedValue;
		return values;
	}

	private String[] convertAreaNeedsToBeChangedAtt(String[] values,
			int rowIndex) {
		String attValue = values[AREA_NEED_TO_BE_CHANGED].toLowerCase();
		String convertedValue = "";
		if (attValue.contains("teaching style")
				|| attValue.contains("syllabus")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Departmental Factor";
		}

		if (attValue.contains("session jam")
				|| attValue.contains("pl duration")
				|| attValue.contains("exam scheduling")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "Adminstrative Factor";
		}
		if (attValue.contains("motivation") || attValue.contains("counselling")) {
			if (convertedValue.length() != 0)

				convertedValue += ":";
			convertedValue = convertedValue + "Motivational Factor";
		}
		if (attValue.contains("present system is alright")) {
			if (convertedValue.length() != 0)
				convertedValue += ":";
			convertedValue = convertedValue + "OK";
		}

		// convertedValue = "Others:";

		// System.out.println(" row: " + rowIndex + "  actual: " + attValue
		// + "  converted: " + convertedValue);
		if(convertedValue.length()==0)
			convertedValue="OK";
		values[AREA_NEED_TO_BE_CHANGED] = convertedValue;
		return values;
	}

	private String[] checkCoCarriculerAttConsistency(String[] values,
			int rowIndex) {
		if (values[CO_CARRICULER_INDEX].contains("No")) {
			if (!values[CO_CARRICULER_TIME_INDEX].contains("N/A")) {
				// System.out
				// .println("Incosistency in co-carriculer attributes No"
				// + " in " + rowIndex);
			}
			values[CO_CARRICULER_TIME_INDEX] = "N/A";
		} else if (values[CO_CARRICULER_INDEX].contains("Yes")) {
			if (values[CO_CARRICULER_TIME_INDEX].length() == 0)
				// System.out
				// .println("Incosistency in co-carriculer attributes yes"
				// + " in " + rowIndex);
				values[CO_CARRICULER_TIME_INDEX] = "0";

		} else {
			// System.out.println("value: " + values[CO_CARRICULER_INDEX]);
		}
		return values;
	}

	private String[] convertCloseFriendInDept(String[] values, int rowIndex) {
		if (values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX].contains("No")) {
			values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX] = "No";
		} else if (values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX].contains("Yes")) {
			values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX] = "Yes";
		}
		if (values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX].length() == 0)
			values[ClOSE_FRIEND_IN_DEPARTMENT_INDEX] = "Yes";

		return values;
	}

	private String[] convertInvolvedInPolitics(String[] values, int rowIndex) {
		if (values[INVOLVED_IN_POLITICS_INDEX].contains("No")) {
			values[INVOLVED_IN_POLITICS_INDEX] = "No";
		} else if (values[INVOLVED_IN_POLITICS_INDEX].contains("Yes")) {
			values[INVOLVED_IN_POLITICS_INDEX] = "Yes";
		}
		if (values[INVOLVED_IN_POLITICS_INDEX].length() == 1)
			values[INVOLVED_IN_POLITICS_INDEX] = "No";

		return values;
	}

	private String[] convertMostTimeSpendPeople(String[] values, int rowIndex) {

		if (values[MOST_TIME_SPEND_PEOPLE_INDEX]
				.contains("Departmental friend")) {
			values[MOST_TIME_SPEND_PEOPLE_INDEX] = "Departmental friend";
		} else if (values[MOST_TIME_SPEND_PEOPLE_INDEX]
				.contains("Non departmental BUET friend")) {
			values[MOST_TIME_SPEND_PEOPLE_INDEX] = "Non departmental BUET friend";
		} else if (values[MOST_TIME_SPEND_PEOPLE_INDEX]
				.contains("Non BUETian friend")) {
			values[MOST_TIME_SPEND_PEOPLE_INDEX] = "Non BUETian friend";
		} else if (values[MOST_TIME_SPEND_PEOPLE_INDEX].contains("Alone")) {
			values[MOST_TIME_SPEND_PEOPLE_INDEX] = "Alone";
		} else
			values[MOST_TIME_SPEND_PEOPLE_INDEX] = "Other";

		return values;
	}

	private void openFile() {
		try {
			fileWriter = new FileWriter(clean_file_name);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void closeFile() {
		try {
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void saveRow(String row) {
		// System.out.println(row);
		try {
			fileWriter.append(row);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printAttributes() {
		for (int i = 0; i < Constants.COLUMN_NO; i++) {
			System.out.println("Attribute " + i + " Size: "
					+ attList.get(i).size());
			if (i == 1) {
				for (int j = 0; j < attList.get(i).size(); j++) {
					System.out.println(attList.get(i).get(j));
				}
				System.out.println("\n\n");
			}
		}
	}

	private void addAttributeinList(String input, int index) {
		if (input.length() > 0) {
			if (!attList.get(index).contains(input))
				attList.get(index).add(input);
		}

	}

	private String convertToCleanAttribute(String att) {
		String str = att.replaceAll("\"", ""); // replacing all " letters
		str = str.replaceAll("\'", ""); // replacing all \' charcaters
		str = str.replaceAll(",", ":"); // replacing all , charcter with :
		// System.out.println(str);
		return str;
	}

	private void saveAttributes() {
		DatabaseHelper dbHelper = new DatabaseHelper();
		for (int i = 0; i < Constants.COLUMN_NO; i++)
			dbHelper.insertDataIntoDatabase(i, attList.get(i));

	}
}

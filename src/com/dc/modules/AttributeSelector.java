package com.dc.modules;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.dc.dtree.DeceesionTreeAlgo;
import com.dc.utility.Constants;

public class AttributeSelector {

	private static final String COMMA_DELIMITER = ",";

	private static final String NEW_LINE_SEPARATOR = "\n";
	private FileWriter fileWriter = null;
	private String clean_file_name = "selected_data.csv";
	private final int GENDER_INDEX = 1;
	private final int BATCH_INDEX = 2;
	private final int DEPARTMENT_INDEX = 3;
	private final int RESIDENCE_INDEX = 4;
	private final int HALL_INDEX = 5;
	private final int COMMNICATION_TIME_INDEX = 6;
	private final int HOW_HALL_LIFE_EFFECT_INDEX = 7;
	private final int CGPA_INDEX = 8;
	private final int CGPA_TREND_INDEX = 9;
	private final int TUITION_ATT_INDEX = 10;
	private final int TUITION_TIME_INDEX = 11;
	private final int TUITION_REASONS_INDEX = 12;
	private final int CO_CARRICULER_INDEX = 13;
	private final int CO_CARRICULER_TIME_INDEX = 14;
	private final int CO_CARRICULER_TYPE_INDEX = 15;
	private final int STUDY_COMPARISON_INDEX = 16;
	private final int GOOD_RESULT_REASONS_INDEX = 17;
	private final int BAD_RESULT_REASONS_INDEX = 18;
	private final int AREA_NEED_TO_BE_CHANGED = 19;
	private final int MOST_TIME_SPEND_STYLE_INDEX = 20;
	private final int ClOSE_FRIEND_IN_DEPARTMENT_INDEX = 21;
	private final int MOST_TIME_SPEND_PEOPLE_INDEX = 22;
	private final int INVOLVED_IN_POLITICS_INDEX = 23;
	private final int POLITICS_EFFECT_INDEX = 24;

	private ArrayList<Integer> attributeList = new ArrayList<Integer>();
	private int CURRENT_CGPA_INDEX;
	private int NUM_OF_COLUMS;
	private int FINAL_ATTRIBUTE_INDEX;

	public AttributeSelector() {
		chooseAttribute();
		NUM_OF_COLUMS = attributeList.size();
		FINAL_ATTRIBUTE_INDEX=attributeList.get(NUM_OF_COLUMS-1);
		Constants.SELECTED_COLUMS=NUM_OF_COLUMS;
		openFile();
		readRawFile();
		closeFile();
		new DeceesionTreeAlgo();
	}

	private void chooseAttribute() {
		attributeList.add(RESIDENCE_INDEX);
		attributeList.add(HOW_HALL_LIFE_EFFECT_INDEX);

		CURRENT_CGPA_INDEX = attributeList.size();
		attributeList.add(CGPA_INDEX);
		attributeList.add(TUITION_ATT_INDEX);
		attributeList.add(TUITION_TIME_INDEX);
		attributeList.add(TUITION_REASONS_INDEX);
		attributeList.add(CO_CARRICULER_INDEX);
		attributeList.add(CO_CARRICULER_TIME_INDEX);
		attributeList.add(CO_CARRICULER_TYPE_INDEX);
		attributeList.add(MOST_TIME_SPEND_STYLE_INDEX);
		attributeList.add(MOST_TIME_SPEND_PEOPLE_INDEX);
		attributeList.add(INVOLVED_IN_POLITICS_INDEX);
	}

	private void readRawFile() {
		int count = 0;
		BufferedReader br = null;
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader("neumeric_data.csv"));
			while ((sCurrentLine = br.readLine()) != null) {

				String[] values = sCurrentLine.split(",", -1);
				// System.out.println(""+values.length+"  "+ sCurrentLine);

				count++;

				String temp = values[CGPA_INDEX];
				values[CGPA_INDEX] = values[FINAL_ATTRIBUTE_INDEX];
				values[FINAL_ATTRIBUTE_INDEX] = temp;
				String row = "";
				for (int j = 0; j < Constants.COLUMN_NO; j++) {
					if (attributeList.contains(j)) {
						String att = values[j];
						row = row + att + COMMA_DELIMITER;
					}
				}
				row=row.substring(0, row.length()-1);
				row = row + NEW_LINE_SEPARATOR;
				//System.out.println("rowIndex: " + count + "   " + row);
				 saveRow(row);

				// if (count > 100)
				// break;
				// creating maps
				// System.out.println("instanc: "+ count);

			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		System.out.println("TOTAL INSTANCES: " + count);
	}
	private void openFile() {
		try {
			fileWriter = new FileWriter(clean_file_name);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void closeFile() {
		try {
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void saveRow(String row) {
		// System.out.println(row);
		try {
			fileWriter.append(row);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

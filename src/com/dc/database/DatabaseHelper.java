package com.dc.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.dc.comparators.SortOrderComparator;
import com.dc.model.TimeSpend;
import com.dc.utility.Constants;
import com.dc.utility.Tablelist;

public class DatabaseHelper {

	private String DATABASE_NAME="buet_data_2";
	private final int MOST_TIME_SPEND_STYLE_INDEX = 20;
	private final int EXTRA_CARRICULER_TYPE_INDEX = 15;
	private final int AREA_NEED_TO_BE_CHANGED = 19;
	private final int HOW_HALL_LIFE_EFFECT_INDEX = 7;
	public DatabaseHelper() {
		// TODO Auto-generated constructor stub
	}

	public void createDatabaseAndTables() {
		String url = "jdbc:mysql://localhost:3306/";

		String user = "root";

		String password = "";
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection(url, user, password);

			Statement stt = con.createStatement();
			
			 stt.execute("CREATE DATABASE IF NOT EXISTS "+DATABASE_NAME);
	         stt.execute("USE "+ DATABASE_NAME);
	         
	         ArrayList<String> tableList=new Tablelist().getTableList();
	         
	         for(int i=0;i<Constants.COLUMN_NO;i++)
	         {
	        	
	        	 stt.execute("DROP TABLE IF EXISTS "+tableList.get(i));
	        	
		            stt.execute("CREATE TABLE "+tableList.get(i) +"(" +
		                    "id BIGINT NOT NULL AUTO_INCREMENT,"
		                    + "att_name TEXT,"
		                    + "status VARCHAR(1),"
		                    + "converted_att TEXT,"
		                    + "PRIMARY KEY(id)"
		                    + ")");
		          
	        	
	         }
	         stt.close();
	         con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void insertDataIntoDatabase(int index,ArrayList<String> dataList)
	{
		ArrayList<String> tableList=new Tablelist().getTableList();
		String url = "jdbc:mysql://localhost:3306/";

		String user = "root";

		String password = "";
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection(url, user, password);

			Statement stt = con.createStatement();
			stt.execute("USE "+ DATABASE_NAME);
			for(int i=0;i<dataList.size();i++){
				
				
				String str=dataList.get(i).replaceAll("\"","");
				str=str.replaceAll("\'","");
				System.out.println(str+ "size  "+ str.length());
				String converted_att="";
				if(index==MOST_TIME_SPEND_STYLE_INDEX)
					converted_att=convertMostTimeSpendAtt(str);
				else if(index==EXTRA_CARRICULER_TYPE_INDEX)
					converted_att=convertCoCarriculerTypeAtt(str);
				else if(index==AREA_NEED_TO_BE_CHANGED)
					converted_att=convertAreaNeedsToBeChangedAtt(str);
				else if(index==HOW_HALL_LIFE_EFFECT_INDEX)
					converted_att=convertHallLifeEffectAtt(str);
				
				stt.execute("INSERT INTO "+ tableList.get(index)+ "(att_name, status,converted_att) VALUES " + 
	                    "('"+str+"','1','"+converted_att+"')");
				}
	} catch (Exception e) {
		e.printStackTrace();
	}
        
	}
	private String convertMostTimeSpendAtt(String values) {
		String attValue=values.toLowerCase();
		String convertedValue = "";
		if (attValue.contains("study")) {
			convertedValue = "Study";
		} else if (attValue.contains("programming")
				|| attValue.contains("competion")
				|| attValue.contains("research")
				|| attValue.contains("robotics")) {

			convertedValue = "Academic Extra Carriculer";

		} else if (attValue.contains("music") || attValue.contains("sports")
				|| attValue.contains("dance")
				|| attValue.contains("organization")
				|| attValue.contains("volunteering")
				|| attValue.contains("books")) {
			convertedValue = "Co carriculer";
		} else
			convertedValue = "Not productive";
		System.out.println(  attValue
				+ "  converted: " + convertedValue);

		return convertedValue;
	}

	private String convertCoCarriculerTypeAtt(String values) {
		String attValue = values.toLowerCase();
		String convertedValue = "";
		if (attValue.contains("contest") || attValue.contains("reseacrh")
				|| attValue.contains("competition")
				|| attValue.contains("robotics") || attValue.contains("software"))
			convertedValue = "Academic";
		else if (attValue.contains("sport") || attValue.contains("music")
				|| attValue.contains("drama") || attValue.contains("dance")
				|| attValue.contains("photography")
				|| attValue.contains("writing") || attValue.contains("work"))
			convertedValue = "Cultural";
		else
			convertedValue = "Non Productive";
		System.out.println(" actual: " + attValue
				+ "  converted: " + convertedValue);

		return convertedValue;
	}
	private String convertAreaNeedsToBeChangedAtt(String values) {
		String attValue = values.toLowerCase();
		String convertedValue = "";
		if (attValue.contains("teaching style")
				|| attValue.contains("syllabus"))
			convertedValue = convertedValue+":"+"Departmental Factor";

		if (attValue.contains("session jam")
				|| attValue.contains("pl duration")
				|| attValue.contains("exam scheduling"))
			convertedValue = convertedValue+":"+ "Adminstrative Factor";
		if (attValue.contains("motivation") || attValue.contains("counselling"))
			convertedValue =convertedValue+":"+  "Motivational Factor:";
		if (attValue.contains("present system is alright"))
			convertedValue = convertedValue+ ":"+"System is OK";
		
		//convertedValue = "Others:";
		if(attValue.length()==0)
			convertedValue="N/A";

		System.out.println("  actual: " + attValue
				+ "  converted: " + convertedValue);

		return convertedValue;
	}
	private String convertHallLifeEffectAtt(String values) {
		String attValue = values.toLowerCase();
		String convertedValue = "";
		if (attValue.contains("problem with") || attValue.contains("discouraging"))
			convertedValue=convertedValue+":"+"Human Factor";
		if(attValue.contains("food") || attValue.contains("hygienic") || attValue.contains("noisy"))
			convertedValue="Adminstrative Factor";
		if(attValue.contains("home sickness") || attValue.contains("privacy issue"))
			convertedValue=convertedValue+":"+"Phsycological Factor";
			
		
		//convertedValue = "Others:";
		if(attValue.length()==0)
			convertedValue="N/A";
		System.out.println( "  actual: " + attValue
				+ "  converted: " + convertedValue);

		return convertedValue;
	}

	public HashMap<String,Integer> raedTableWithSortOrder(String tablename)
	{
		HashMap<String,Integer> map=new HashMap<String, Integer>();
		String url = "jdbc:mysql://localhost:3306/";

		String user = "root";

		String password = "";
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection(url, user, password);

			Statement stt = con.createStatement();
			stt.execute("USE "+ DATABASE_NAME);
			
			 ResultSet res = stt.executeQuery("SELECT * FROM "+tablename);
			 while (res.next())
	            {
	             
	               map.put(res.getString("att_name"), res.getInt("sort_order"));
	             
	            }
			
	} catch (Exception e) {
		e.printStackTrace();
	}
		
		return map;
        
	}
}

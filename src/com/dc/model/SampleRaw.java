package com.dc.model;

public class SampleRaw {
	
	private String gender;
	private String batch;
	private String residence;
	private int cgpa;
	private String didTuition;
	private String tuitionTime;
	private String didExtraCarriculer;
	private String extraCarriculerTime;
	private String closeFriendInDept;
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getResidence() {
		return residence;
	}
	public void setResidence(String residence) {
		this.residence = residence;
	}
	public int getCgpa() {
		return cgpa;
	}
	public void setCgpa(int cgpa) {
		this.cgpa = cgpa;
	}
	public String getDidTuition() {
		return didTuition;
	}
	public void setDidTuition(String didTuition) {
		this.didTuition = didTuition;
	}
	public String getTuitionTime() {
		return tuitionTime;
	}
	public void setTuitionTime(String tuitionTime) {
		this.tuitionTime = tuitionTime;
	}
	public String getDidExtraCarriculer() {
		return didExtraCarriculer;
	}
	public void setDidExtraCarriculer(String didExtraCarriculer) {
		this.didExtraCarriculer = didExtraCarriculer;
	}
	public String getExtraCarriculerTime() {
		return extraCarriculerTime;
	}
	public void setExtraCarriculerTime(String extraCarriculerTime) {
		this.extraCarriculerTime = extraCarriculerTime;
	}
	public String getCloseFriendInDept() {
		return closeFriendInDept;
	}
	public void setCloseFriendInDept(String closeFriendInDept) {
		this.closeFriendInDept = closeFriendInDept;
	}
	
	
}
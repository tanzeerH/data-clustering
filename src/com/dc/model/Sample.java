package com.dc.model;

public class Sample {
	
	private String gender;
	private String batch;
	private String residence;
	private double cgpa;
	private String didTuition;
	private double tuitionTime;
	private String didExtraCarriculer;
	private double extraCarriculerTime;
	private String closeFriendInDept;
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getResidence() {
		return residence;
	}
	public void setResidence(String residence) {
		this.residence = residence;
	}
	public double getCgpa() {
		return cgpa;
	}
	public void setCgpa(double cgpa) {
		this.cgpa = cgpa;
	}
	public String getDidTuition() {
		return didTuition;
	}
	public void setDidTuition(String didTuition) {
		this.didTuition = didTuition;
	}
	public double getTuitionTime() {
		return tuitionTime;
	}
	public void setTuitionTime(double tuitionTime) {
		this.tuitionTime = tuitionTime;
	}
	public String getDidExtraCarriculer() {
		return didExtraCarriculer;
	}
	public void setDidExtraCarriculer(String didExtraCarriculer) {
		this.didExtraCarriculer = didExtraCarriculer;
	}
	public double getExtraCarriculerTime() {
		return extraCarriculerTime;
	}
	public void setExtraCarriculerTime(double extraCarriculerTime) {
		this.extraCarriculerTime = extraCarriculerTime;
	}
	public String getCloseFriendInDept() {
		return closeFriendInDept;
	}
	public void setCloseFriendInDept(String closeFriendInDept) {
		this.closeFriendInDept = closeFriendInDept;
	}
	
	
}
package com.dc.model;

public class NominalKeys {
	
	private String KEY_GENDER="key_gender";
	private String KEY_BATCH="key_batch";
	private String KEY_RESIDENCE="key_residence";
	private String KEY_DID_TUITION="key_did_tuition";
	private String KEY_DID_EXTRA_CARRICULER="key_extra_carriculer";
	private String KEY_HAD_FRIEND_IN_DEPT="key_had_friend_in_dept";
	
	public NominalKeys()
	{
	}

	public String getKEY_GENDER() {
		return KEY_GENDER;
	}

	public void setKEY_GENDER(String kEY_GENDER) {
		KEY_GENDER = kEY_GENDER;
	}

	public String getKEY_BATCH() {
		return KEY_BATCH;
	}

	public void setKEY_BATCH(String kEY_BATCH) {
		KEY_BATCH = kEY_BATCH;
	}

	public String getKEY_RESIDENCE() {
		return KEY_RESIDENCE;
	}

	public void setKEY_RESIDENCE(String kEY_RESIDENCE) {
		KEY_RESIDENCE = kEY_RESIDENCE;
	}

	public String getKEY_DID_TUITION() {
		return KEY_DID_TUITION;
	}

	public void setKEY_DID_TUITION(String kEY_DID_TUITION) {
		KEY_DID_TUITION = kEY_DID_TUITION;
	}

	public String getKEY_DID_EXTRA_CARRICULER() {
		return KEY_DID_EXTRA_CARRICULER;
	}

	public void setKEY_DID_EXTRA_CARRICULER(String kEY_DID_EXTRA_CARRICULER) {
		KEY_DID_EXTRA_CARRICULER = kEY_DID_EXTRA_CARRICULER;
	}

	public String getKEY_HAD_FRIEND_IN_DEPT() {
		return KEY_HAD_FRIEND_IN_DEPT;
	}

	public void setKEY_HAD_FRIEND_IN_DEPT(String kEY_HAD_FRIEND_IN_DEPT) {
		KEY_HAD_FRIEND_IN_DEPT = kEY_HAD_FRIEND_IN_DEPT;
	}

}

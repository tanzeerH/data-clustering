package com.dc.model;

public class Distance {
	
	private int clusterIndex;
	private double distance;
	public Distance(int clusterIndex, double distance) {
		super();
		this.clusterIndex = clusterIndex;
		this.distance = distance;
	}
	public int getClusterIndex() {
		return clusterIndex;
	}
	public void setClusterIndex(int clusterIndex) {
		this.clusterIndex = clusterIndex;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	

}

package com.dc.model;

public class TimeSpend {
	
	private String timeSpend;
	private int sortOrder;
	public String getTimeSpend() {
		return timeSpend;
	}
	public void setTimeSpend(String timeSpend) {
		this.timeSpend = timeSpend;
	}
	public int getSortOrder() {
		return sortOrder;
	}
	public TimeSpend(String timeSpend, int sortOrder) {
		super();
		this.timeSpend = timeSpend;
		this.sortOrder = sortOrder;
	}
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

}

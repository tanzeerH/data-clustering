package com.dc.model;

import java.awt.Label;
import java.util.ArrayList;

public class TreeNode {

	private String attribute;
	private String attributeValue="-1";
	
	

	private ArrayList<TreeNode> childList=new ArrayList<TreeNode>();
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	public ArrayList<TreeNode> getChildList() {
		return childList;
	}
	public void setChildList(ArrayList<TreeNode> childList) {
		this.childList = childList;
	}
}
